# GKE & Gitlab Integration

## GitOps 
This project uses terraform and allows for GKE to be integrated with Gitlab.

### Structure:
```
├── .gitlab-ci.yml     # Calls out to 'parabol-gitops/infra/templates/terraform.gitlab-ci.yml' for execution of tf stages.
├── backend.tf         # State file Location Configuration
├── gitlab-admin.tf    # Adding kubernetes service account
├── gke.tf             # Google GKE Configuration
└── group_cluster.tf   # Registering kubernetes cluster to GitLab `apps` Group
└── outputs.tf         # Generates URL from pipeline
└── variables.tf       # Add variables to abstract values
└── versions.tf        # Adding providers
```

